function doubleItem (collection) {
  // Need to be implemented
  return collection.map(number => number * 2);
}

function doubleEvenItem (collection) {
  // Need to be implemented
  return collection.filter(number => number % 2 === 0).map(number => number * 2);
}

function covertToCharArray (collection) {
  // Need to be implemented
  return collection.map(number => String.fromCharCode(number + 96));
}

function getOneClassScoreByASC (collection) {
  // Need to be implemented
  return collection.filter(classes => classes.class === 1).map(scores => scores.score).sort();
}

export { doubleItem, doubleEvenItem, covertToCharArray, getOneClassScoreByASC };
