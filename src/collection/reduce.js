function getMaxNumber (collction) {
  // Need to be implemented
  return collction.reduce((a, b) => {
    if (a > b) {
      return a;
    } else {
      return b;
    }
  });
}

function isSameCollection (collction1, collction2) {
  // Need to be implemented
  if (collction1.length !== collction2.length) {
    return false;
  }
  let result = true;
  collction1.reduce((acc, cur, index) => {
    if (cur !== collction2[index]) {
      result = false;
    }
  });
  return result;
}
// The acc parameter in reduce function is not used. Is it possible to implement map function here?

function sum (collction) {
  // Need to be implemented
  return collction.reduce((sum, number) => sum + number);
}

function computeAverage (collction) {
  // Need to be implemented
  return sum(collction) / collction.length;
}

function lastEven (collction) {
  // Need to be implemented
  return collction.filter(number => number % 2 === 0)[collction.filter(number => number % 2 === 0).length - 1];
}

export { getMaxNumber, isSameCollection, computeAverage, sum, lastEven };
