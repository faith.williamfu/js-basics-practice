function getAllEvens (collection) {
  // Need to be implemented
  return collection.filter(number => number % 2 === 0);
}

function getAllIncrementEvens (start, end) {
  // Need to be implemented
  const arr = [];
  for (; start <= end; start++) {
    arr.push(start);
  }
  return arr.filter(number => number % 2 === 0);
}

function getIntersectionOfcollections (collection1, collection2) {
  // Need to be implemented
  return collection1.filter((number) => collection2.indexOf(number) !== -1);
}

function getUnionOfcollections (collection1, collection2) {
  // Need to be implemented
  return collection1.concat(collection2.filter((number) => collection1.indexOf(number) === -1));
}

function countItems (collection) {
  // Need to be implemented
  const withoutDuplication = [];
  collection.forEach(element => {
    if (withoutDuplication.indexOf(element) === -1) {
      withoutDuplication.push(element);
    }
  });
  const times = new Array(withoutDuplication.length);
  for (let i = 0; i < times.length; i++) {
    times[i] = 0;
  }
  for (let i = 0; i < withoutDuplication.length; i++) {
    for (let j = 0; j < collection.length; j++) {
      if (withoutDuplication[i] === collection[j]) {
        times[i]++;
      }
    }
  }
  const output = {};
  for (let i = 0; i < withoutDuplication.length; i++) {
    output[withoutDuplication[i]] = times[i];
  }
  return output;
}

export {
  getAllEvens,
  getAllIncrementEvens,
  getIntersectionOfcollections,
  getUnionOfcollections,
  countItems
};
