function getDayOfMonth (dateStr) {
  // Need to be implemented
  const date = new Date(dateStr);
  return date.getDate();
}

function getDayOfWeek (dateStr) {
  // Need to be implemented
  const date = new Date(dateStr);
  return date.getDay();
}

function getYear (dateStr) {
  // Need to be implemented
  const date = new Date(dateStr);
  return date.getFullYear();
}

function getMonth (dateStr) {
  // Need to be implemented
  const date = new Date(dateStr);
  return date.getMonth();
}

function getMilliseconds (dateStr) {
  // Need to be implemented
  const date = new Date(dateStr);
  return date.getTime();
}

export { getDayOfMonth, getDayOfWeek, getYear, getMonth, getMilliseconds };
