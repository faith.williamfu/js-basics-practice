function flatArray (arr) {
  // Need to be implemented
  const newArr = arr.flat();
  return newArr;
}

function aggregateArray (arr) {
  // Need to be implemented
  const newArr = arr.flatMap(number => [[number * 2]]);
  return newArr;
}

function getEnumerableProperties (obj) {
  // Need to be implemented
  return Object.getOwnPropertyNames(obj);
}

function removeDuplicateItems (arr) {
  // Need to be implemented
  return Array.from(new Set(arr));
}

function removeDuplicateChar (str) {
  // Need to be implemented
  const charArr = str.split('');
  return Array.from(new Set(charArr)).join('');
}

function addItemToSet (set, item) {
  // Need to be implemented
  return set.add(item);
}

function removeItemToSet (set, item) {
  // Need to be implemented
  set.delete(item);
  return set;
}

function countItems (arr) {
  // Need to be implemented
  const numberSet = new Set(arr);
  const numberMap = new Map();
  numberSet.forEach(
    (value) => numberMap.set(value, 0)
  );
  numberMap.forEach(
    (value, key, set) => {
      for (let i = 0; i <= arr.length; i++) {
        if (arr[i] === key) {
          set.set(key, ++value);
        }
      }
    });
  return numberMap;
}

export {
  flatArray,
  aggregateArray,
  getEnumerableProperties,
  removeDuplicateItems,
  removeDuplicateChar,
  addItemToSet,
  removeItemToSet,
  countItems
};
